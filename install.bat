@echo off

rem Clone the git repository to the current directory
git clone https://github.com/user/repo.git

rem Change to the cloned directory
cd repo

rem Create a new virtual environment named "env"
python -m venv env

rem Activate the virtual environment
env\Scripts\activate.bat

rem Install the requirements from the requirements.txt file
pip install -r requirements.txt

