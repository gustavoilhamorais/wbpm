# Windows Bitcoin Price Monitor (WBPM)

<div align="left">
  <a href="https://python.org" title="Python website" alt="python">
    <img src="https://img.shields.io/badge/python-3.7-green" width="70" alt="python-version">
  </a>
  <a href="https://pypi.org/project/win10toast/" title="PyPi Win10 Toast" alt="pypi-win10toast">
    <img src="https://img.shields.io/badge/win10toast-0.9-royalblue" width="90" alt="python-version">
  </a>
  <a href="https://pypi.org/project/requests/" title="PyPi Requests" alt="pypi-requests">
    <img src="https://img.shields.io/badge/requests-2.28.1-pink" width="95" alt="python-version">
  </a>
</div>

<br>

## Description
This script will notify you when BTC price is equal or lower than the value defined by the user on the startup questionary.
The user can define the interval between each BTC's price consult and how much BRL (brazilian reais) should trigger the notification.


### Windows dependencies
If you already don't have python installed in your Windows machine, follow the instructions in the [official webstite](https://www.python.org) before continuing.

You must have GIT installed in your windows machine to do this process. You can download it [here](https://git-scm.com/download/win), in it's [official website](https://git-scm.com).


## Automatic installation
The project has a [batch script](https://gitlab.com/gustavoilhamorais/wbpm/-/blob/main/install.bat) to make the manual section steps automatically. You can just execute it as any windows program and it should just let everything ready for you to jump to "running application" section.


## Manual installation

### First, clone this repository:
To clone the repository, use the following command in a terminal emulator, inside the folder you wish to store the application:

```
git clone git@gitlab.com:gustavoilhamorais/wbpm.git
```


### Installing the requirements
Once you're inside the application directory in your terminal emulator, run the following command:

```
python -m venv venv
```
Then, activate the new python environment with:
```
.\venv\Scripts\activate
```
Now, install the requirements in the fresh environment.
```
pip install -r .\requirements.txt
```

### Running the application
Just run the following command in a terminal and answer some configuration inputs before letting the app accomplish it's purpose:
```
python main.py
```

### LICENSE
<strong>MPL-2.0</strong>
<br>
Any person is allowed to copy and/or modify this code to make it serve in any kind of purpose without having to concern with credits, market shares, licensing or any legal topic when it comes to this code's author. In the same way, the author of this script is not responsible for any action or use of this tool and do not guarantee anything about this software. Part of this content was writting by other people and were not verified by the author and are not under the author's responsibilities.