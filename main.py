import requests
import win10toast
import time


def header():
    print("\n\n")
    print(50 * "#")
    print("₿ ₿ ₿ Notify BTC Price ₿ ₿ ₿")
    print("Language: pt-BR")
    print("Version: 1.0.1")
    print("Author: gustavoilhamorais")
    print(50 * "#")
    print("\n\n")


def get_btc_price():
    url = "https://www.mercadobitcoin.net/api/BTC/ticker/"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        return float(data["ticker"]["last"])


def handle_notification(GOAL, interval):
    while True:
        btc_price = get_btc_price()
        if btc_price <= GOAL:
            toaster = win10toast.ToastNotifier()
            toaster.show_toast("BTC Alert", f"BTC reached R${btc_price}", duration=10)
            time.sleep(interval)


def start():
    header()
    GOAL = int(input("Qual valor deve acionar a notificação?\n-> "))
    interval = int(input("A cada quantos segundos a informação deverá ser atualizada?\n-> "))
    handle_notification(GOAL, interval)


start()
